#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

#define HEAP_SIZE 8175
#define BLOCK_SIZE 1000

static void heap_free(void *heap)
{
	munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}

static struct block_header *get_header(void *contents)
{
	return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

bool malloc_block()
{
	fprintf(stdout, "\n\n\tTest malloc_block\n\n____Init heap____\n");
	void *heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout, heap_addr);
	if (heap_addr == NULL)
	{
		fprintf(stderr, "\tHeap init error\n");
		heap_free(heap_addr);
		return false;
	}
	fprintf(stdout, "\n____Memory allocation____\n");
	void *content = _malloc(BLOCK_SIZE);
	struct block_header *block = get_header(content);
	debug_heap(stdout, heap_addr);
	if (block->capacity.bytes != BLOCK_SIZE || content == NULL)
	{
		fprintf(stderr, "\tMemory allocation error\n");
		heap_free(heap_addr);
		return false;
	}
	fprintf(stdout, "\n____Free memory____\n");
	_free(content);
	debug_heap(stdout, heap_addr);
	if (!block->is_free)
	{
		fprintf(stderr, "\tMemory release error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool free_one_block()
{
	fprintf(stdout, "\n\n\tTest free_one_block\n\n____Init heap____\n");
	void *heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Memory allocation____\n");
	_malloc(BLOCK_SIZE);
	void *second_content = _malloc(BLOCK_SIZE);
	_malloc(BLOCK_SIZE);
	struct block_header *second_block = get_header(second_content);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Free second block____\n");
	_free(second_content);
	debug_heap(stdout, heap_addr);
	if (!second_block->is_free)
	{
		fprintf(stderr, "\tMemory release error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool free_some_blocks()
{
	fprintf(stdout, "\n\n\tTest free_some_blocks\n\n____Init heap____\n");
	void *heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Memory allocation____\n");
	_malloc(BLOCK_SIZE);
	void *second_content = _malloc(BLOCK_SIZE);
	void *third_content = _malloc(BLOCK_SIZE);
	_malloc(BLOCK_SIZE);
	struct block_header *second_block = get_header(second_content);
	struct block_header *third_block = get_header(third_content);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Free second block____\n");
	_free(second_content);
	_free(third_content);
	debug_heap(stdout, heap_addr);
	if (!second_block->is_free || !third_block->is_free)
	{
		fprintf(stderr, "\tMemory release error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool expand_region()
{
	fprintf(stdout, "\n\n\tTest expand_region\n\n____Init heap____\n");
	void *heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Memory allocation____\n");
	void *first_content = _malloc(HEAP_SIZE);
	struct block_header *first_block = get_header(first_content);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Expand region____\n");
	void *second_content = _malloc(BLOCK_SIZE);
	struct block_header *second_block = get_header(second_content);
	debug_heap(stdout, heap_addr);
	if (second_content == NULL || first_block->next != second_block || second_block != (void *)first_block->contents + first_block->capacity.bytes)
	{
		fprintf(stderr, "\tRegion expansion error\n");
		heap_free(heap_addr);
		return false;
	}
	heap_free(heap_addr);
	return true;
}

bool init_second_region()
{
	fprintf(stdout, "\n\n\tTest init_second_region\n\n____Init heap____\n");
	void *heap_addr = heap_init(HEAP_SIZE);
	debug_heap(stdout, heap_addr);
	fprintf(stdout, "\n____Memory allocation____\n");
	void *first_content = _malloc(HEAP_SIZE);
	struct block_header *first_block = get_header(first_content);
	debug_heap(stdout, heap_addr);
	void *after_block = (void *)first_block->contents + first_block->capacity.bytes;
	void *mapped_region = mmap(after_block, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
	if (mapped_region == MAP_FAILED)
	{
		fprintf(stderr, "\tRegion mapping error\n");
		heap_free(heap_addr);
		return false;
	}
	fprintf(stdout, "\n____Create second region____\n");
	void *second_content = _malloc(BLOCK_SIZE);
	struct block_header *second_block = get_header(second_content);
	debug_heap(stdout, heap_addr);
	if (second_content == NULL || second_block->capacity.bytes != BLOCK_SIZE || second_block == (void *)first_block->contents + first_block->capacity.bytes)
	{
		fprintf(stderr, "\tRegion mapping error\n");
		heap_free(heap_addr);
		munmap(mapped_region, HEAP_SIZE);
		return false;
	}
	heap_free(heap_addr);
	munmap(mapped_region, HEAP_SIZE);
	return true;
}

size_t tests_count = 5;
my_test tests[] = {
	malloc_block,
	free_one_block,
	free_some_blocks,
	expand_region,
	init_second_region};

bool tester()
{
	size_t tests_passed = 0;
	for (size_t i = 0; i < tests_count; i++)
	{
		bool result = tests[i]();
		tests_passed += result;
		printf("\n -----------------    ");
		printf("Test #%" PRId64 " %s", i + 1, result ? "succeed" : "failed");
		printf("    ----------------- \n");
	}
	printf("\n ---------------------- %" PRId64 "/5 tests passed ----------------------\n", tests_count);
	return tests_passed == 5 ? true : false;
}
